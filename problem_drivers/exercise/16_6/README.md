# 16_6 比较日期 Modify date

## 题目

修改第 5 章的编程题 9，以便用户输入的日期都存储在一个 date 结构（见练习题 5）中。把练习题 5 中
的 compare_dates 函数集成到你的程序中。

## 样例

### 样例一

    Enter first date (mm/dd/yy): 3/6/08
    Enter second date (mm/dd/yy): 5/17/07
    5/17/07 is earlier than 3/6/08

### 样例二

    Enter first date (mm/dd/yy): 4/5/11
    Enter second date (mm/dd/yy): 5/3/12
    4/5/11 is earlier than 5/3/12

## 数据范围

输入的 date 中 mm 范围为 1-12，dd 范围为 1-31, yy 范围为 1-99。