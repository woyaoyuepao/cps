#include<stdio.h>
#define NAME_SIZE 100
int main()
{   
    char first_name[NAME_SIZE] = {'\0'};
    char last_name[NAME_SIZE] = {'\0'};
    printf("Enter a first and last name: ");
    // 将用户输入的姓名字符串分别存储起来，并且跳过空白格
    scanf(" %s %s", first_name, last_name);
    // 打印last name 和 first name 的第一个字母
    printf("%s, %c.\n", last_name, first_name[0]);
    return 0;
}