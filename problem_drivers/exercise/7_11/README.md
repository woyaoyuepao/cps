# 7_11 打印名字

## 题目

编写一个程序，根据用户输入的英文名和姓先显示姓氏，其后跟一个逗号，然后显示名的首字母，最后加一个点：

        Enter a first and last name: Lloyd Fosdick
        Fosdick, L.

用户的输入中可能包含空格（名之前、名和姓之间、姓氏之后）。

## 样例

### 样例一

        Enter a first and last name: Lloyd Fosdick
        Fosdick, L.

### 样例二

        Enter a first and last name: Bird Li
        Li, B.

## 数据范围

1. 输入的数据应为一段英文;

## 提示

1. 按照题目内容中的输出为规范格式进行输出。

2. 记得跳过多余的空格