# 27_1_2 修改quadratic.c Modify quadratic.c

## 题目

(C99)对 27.4 节的 quadratic.c 程序做如下修改:(b) 让程序在显示根的值之前对判别式进行测试。如果判别式为负，按以前的方式显示根的值；如果判别式非负，以实数（无虚部）的形式显示根的值。

## 样例

### 样例一

   Please Enter the value of a,b,c: 1,1,-2

   root1 = 1
   root2 = -2

### 样例二

   Please Enter the value of a,b,c: 5,2,1

   root1 = -0.2 + 0.4i
   root2 = -0.2 + -0.4i

## 数据范围

a,b,c 取值范围均在'double'类型内。
输出的 root1, root2 为'%g'类型。