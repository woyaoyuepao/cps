# 27_3 笛卡尔形式 Cartesian form

## 题目

(C99) 编写程序，把用极坐标表示的复数转换为笛卡尔形式。用户输入 r 和 θ 的值，程序以 a+bi 的形式显示该数，其中 
a=rcosθ
b=rsinθ

## 样例

### 样例一

Please Enter the value of r,θ: 2,90

Cartesian form: 2.00 + 0.00i

### 样例二

Please Enter the value of r,θ: 3,150

Cartesian form: 1.50 + -2.60i

## 数据范围

输入的r和θ的取值范围在'double'类型内。
输出的a,b同时满足'double'，不过需要保留两位小数。