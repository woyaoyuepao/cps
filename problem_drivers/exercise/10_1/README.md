<<<<<<< HEAD
##第十章

1. 修改 10.2 节的栈实例使它存储字符而不是整数。接下来，增加 main 整数，用来要求用户输入一串圆括号或滑括号，然后指出它们之间的嵌套是否正确：

        Enter parenteses and/or braces: ((){}{()})
        Parenteses/braces are nested properly

=======
##第十章

1. 修改 10.2 节的栈实例使它存储字符而不是整数。接下来，增加 main 整数，用来要求用户输入一串圆括号或滑括号，然后指出它们之间的嵌套是否正确：

        Enter parenteses and/or braces: ((){}{()})
        Parenteses/braces are nested properly

>>>>>>> b72d3cd3e8750459ed4fa975d1a92b4b2b00dcb6
    提示：读入左圆括号或左花括号时，把它们像字符一样压入栈中。当读入右圆括号或右花括号时，把栈顶的顶弹出，并且检查弹出项是否是匹配的圆括号或花括号。（如果不是，那么圆括号或者花括号嵌套不正确）。当程序读入换行符时，检查栈是否为空。如果为空，那么圆括号或者花括号匹配；如果栈不为空（或者如果曾经调用过 stack_underflow 函数），那么圆括号或花括号不匹配。如果调用 stack_overflow 函数，程序显示信息 Stack overflow，并且立刻终止。