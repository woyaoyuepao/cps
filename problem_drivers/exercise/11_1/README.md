### 11-1：

 修改第 2 章的编程题 7，使其包含下列函数：

		void pay_amount(int dollars, int *twenties, int *tens, int *fives, int *ones);

函数需要确定：为支付参数 dollars 表示的付款金额，所需 20美元、10美元、5美元和 1美元的最小数目。twenties 参数所指向的变量存储所需要 20美元的数目，tens、fives 和 ones 参数类似。
