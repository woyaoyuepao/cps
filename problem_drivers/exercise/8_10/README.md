# 8_10 修改5_8 Modify 5_8

## 题目

修改第 5 章编程题 8 ，用一个数组存储起飞时间，另一个数组存储抵达时间。（时间用整数表示，表示从午夜开始的分钟数。）程序用一个循环搜索起飞时间数组，以找到与用户输入的时间最接近的起飞时间。

## 样例

### 样例一

    Enter a 24-hour time: 14:00
    Closest departure time is 2:00 p.m., arriving at 4:08 p.m.

### 样例二

    Enter a 24-hour time: 13:15
    Closest departure time is 12:47 p.m., arriving at 3:00 p.m.

### 样例三

    Enter a 24-hour time: 1:23
    Closest departure time is 9:45 p.m., arriving at 11:58 p.m.

## 数据范围

输入数据均为24小时制时间。
