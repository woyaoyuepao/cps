# 26_4 显示日期 Output date

## 题目

编写一个程序，提示用户录入一个日期（月，日和年）和一个整数 n，然后显示 n 天后的日期。

## 样例

### 样例一

Enter month (1-12): 3
Enter day (1-31): 30
Enter year: 2018
Enter number of days in future: 1

Future date: 3 /31 /2018

### 样例二

Enter month (1-12): 2
Enter day (1-31): 24
Enter year: 2004
Enter number of days in future: 55

Future date: 4 /19 /2004

## 数据范围

日期要考虑到每个月的不同天数和是否为闰年的二月份天数。
按照规范的格式进行输入输出。

## 提示

可以使用时间头文件 time.h。