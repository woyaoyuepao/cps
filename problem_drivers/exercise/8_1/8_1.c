#include<stdio.h>
#define BUFF_SIZE 100
#define DIGIT_NUM 10

int get_line(char *);

int main(){
    char buffer[BUFF_SIZE];
    printf("Enter a number: ");
    int length = get_line(buffer);
    int num[DIGIT_NUM];
    int val;
    printf("Repeated digit(s):");
    for(int i = 0; i < DIGIT_NUM; i++)
        num[i] = 0;
    /*initialize*/
    for(int j = 0; j < length; j++){
        val = buffer[j] - '0'; //get the value of character
        num[val]++;
        /* if(num[val] == 1) */
            /* printf(" %d",val);    */
    }
    for(int k = 0; k < DIGIT_NUM; k++)
        if(num[k] > 1)
            printf(" %d",k);
    printf("\n");
    return 0;
}

int get_line(char *s){
    int c;
    int i = 0;
    while((c = getc(stdin)) != '\n'){
        i++;
        *s++ = c;
    }
    *s = '\0';
    return i;
}

