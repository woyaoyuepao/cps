#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define BUFFERSIZE 50

void replace(char *temp);

int main(int argc, char *argv)
{
	char buffer[BUFFERSIZE];
	memset(buffer, 0, BUFFERSIZE);
	int length;

    printf("Enter message: ");
	fgets(buffer, sizeof(buffer), stdin);
    length = strlen(buffer);

    for (int i = 0; i < length - 1; i++)
    	buffer[i] = toupper(buffer[i]);

    for(int i = 0; i < length - 1; i++)
    	replace(&buffer[i]);

    for(int i = length -1; i < length + 9; i++)
    	buffer[i] = '!';
    
    buffer[length + 9] = '\0';

    printf("In B1FF-speak: %s\n", buffer);

}

void replace(char *temp)
{
	char c = *temp;

	switch (c) {
		case 'A': c = '4';
				  break;
	    case 'B': c = '8';
				  break;
	    case 'E': c = '3';
				  break;
		case 'I': c = '1';
				  break;
		case 'O': c = '0';
				  break;
		case 'S': c = '5';
				  break;
		default:  break;
	}

	*temp = c;
}
