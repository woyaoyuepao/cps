#include <stdio.h>
#include <stdlib.h>

#define MAX_NUMS 10000

int compare_nums(const void *p1, const void *p2);

int main(int argc, char *argv[])
{
    FILE *fp;
    int i = 0, numbers[MAX_NUMS];
    int median;
    if (argc != 2) {
        fprintf(stderr, "Usage: programname inputfile.\n");
        exit(EXIT_FAILURE);
    }

    if ((fp = fopen(argv[1], "r")) == NULL) {
        fprintf(stderr, "Cannot open file %s.\n", argv[1]);
        exit(EXIT_FAILURE);
    }

    while (!feof(fp) && i < MAX_NUMS) {
        fscanf(fp, " %d", &numbers[i++]);
    }
    int length = i - 1;
    qsort(numbers, length, sizeof(int), compare_nums);
    if (length % 2 == 0) {
        median = (int)(numbers[length/2 - 1] + numbers[length / 2]) / 2;
    } else {
        median = numbers[(length - 1)/2];
    }
    printf("Lowest: %d\n", numbers[0]);
    printf("Median: %d\n", median);
    printf("Highest: %d\n", numbers[length - 1]);
    fclose(fp);
    return 0;
}

int compare_nums(const void *p1, const void *p2)
{
    return *((int *)p1) - *((int *)p2);
}
