#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define BUFFERSIZE 40
#define SIZE 12

int main(int argc, char *argv)
{
	char buffer[BUFFERSIZE];
	memset(buffer, 0, BUFFERSIZE);
	int length;
	char number[SIZE];
	int digit[SIZE];
	char temp;
	int result;

    printf("Enter the first 12 digits of an EAN: ");
	fgets(buffer, sizeof(buffer), stdin);
    length = strlen(buffer);
    buffer[length] = '\0';
    sscanf(buffer, "%s", number);

    for(int i = 0; i < SIZE; i++){
    	char temp = number[i];
    	digit[i] = atoi(&temp);
    }
   
    for(int i = 1; i <= 11; i += 2)
    	result += digit[i];

    result *= 3;

    for(int i = 0; i <= 10; i += 2)
    	result += digit[i];

    printf("Check digit: %d\n", 9 - ((result - 1) % 10));
}