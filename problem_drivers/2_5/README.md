# 最大公约数 Calculate GCD

## 题目编号 6-2

## 题目

编写程序，要求用户输入两个整数，然后计算这两个整数的最大公约数（ GCD ）。

## 样例

### 样例一

    Enter two integers：12 28
    Greatest common divisor：4

### 样例二

    Enter two integers：1 9
    Greatest common divisor：1

## 数据范围

输入的两个整数均为 int 型的值，输出的最大公约数也为 int 型的值。

## 提示

求最大公约数的经典算法是 Euclid 算法，方法如下：
分别让变量 m 和 n 存储两个数的值。如果 n 为 0，那么停止操作，m 中的值是 GCD ；
否则计算 m 除以 n 的余数，把 n 保存到 m 中，并把余数保存到 n 中。重复上述操作。
