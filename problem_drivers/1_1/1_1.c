#include <stdio.h>

float amount;

int main(void)
{
    printf("Enter an amount: ");
    scanf("%f", &amount);
    printf("With tax added: $%.2f\n", amount * 1.05f);

    return 0;
}
