# 股经纪人的佣金 Commission Stock Broker

## 题目编号 6-4

## 题目

在 5.2 节的 broker.c 程序中添加循环，以便用户可以输入多笔交易并且程序可以计算每
次的佣金。程序在用户输入的交易额为 0 时终止。

## 样例

### 样例一

    Enter value of trade：30000
    Commission：$166.00

    Enter value of trade：20000
    Commission：$144.00

    Enter value of trade：0

### 样例二

    Enter value of trade：526
    Commission：$39.00

    Enter value of trade：0

## 数据范围

输入的交易额为大于 0 的小数。输出保留两位小数的佣金。


## 测试用例

2500\\n3000\\n4000\\n5000\\n6249\\n0\\n
6250\\n7000\\n8000\\n15000\\n19999\\n0\\n
20000\\n30000\\n35000\\n40000\\n49999\\n0\\n
50000\\n100000\\n200000\\n300000\\n490000\\n0\\n
600000\\n700000\\n800000\\n900000\\n1000000\\n0\\n
50\\n100\\n200\\n300\\n529\\n0\\n
